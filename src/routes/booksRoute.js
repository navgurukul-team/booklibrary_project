const bookController = require('../controllers/booksControllers')
const { } = require('../middleware/auth')
const express = require('express')
const router = express.Router()

router.post('/signUp', bookController.signUpFn)

router.post('/login', bookController.loginFn)

router.get('/viewAllMembers', bookController.viewAllMember)

router.post('/addnewmember', bookController.addNewMember)

router.post('/removeMember', bookController.removeMember)

router.post('/upDateInfo', bookController.upDateInfo)

router.post('/addNewbook', bookController.addNewBook)

router.get('/listAllBook', bookController.listAllBook)

router.post('/removeBook', bookController.removeBook)

router.post('/updateBook', bookController.updateBook)

router.post('/bookIssue', bookController.issueBook)

router.post('/returnBook', bookController.returnBook)



module.exports = router
