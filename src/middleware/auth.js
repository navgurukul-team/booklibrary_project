const env = require('dotenv').config()
const jwt = require('jsonwebtoken')
const secretKey = "@books025"

const createToken = (email)=>{
    return jwt.sign({email} , process.env.secretKey || secretKey ,{expiresIn: '24h'})
}
console.log(process.env.secretKey);

const verifyToken = async(req , res , next )=>{
    try {
        if(req.headers.cookie){
            const token = req.headers.cookie.split('=')[1]
            const check = (token ,secretKey)
            req.check = check
            next()
        }
        else{
            res.status(400).json({message:"please login your account..."})
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({error:"internal issues..."})
    }
}


module.exports = { createToken , verifyToken }